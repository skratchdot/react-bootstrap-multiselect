/* generated by gulpfile.js */
module.exports = exports = function () {
	return [
		"allSelectedText",
		"buttonClass",
		"buttonContainer",
		"buttonText",
		"delimiterText",
		"disableIfEmpty",
		"dropUp",
		"enableCaseInsensitiveFiltering",
		"enableClickableOptGroups",
		"filterBehavior",
		"includeSelectAllOption",
		"multiple",
		"nonSelectedText",
		"onChange",
		"onDropdownShow",
		"onDropdownShown",
		"onSelectAll",
		"optionClass",
		"selectAllName",
		"selectAllText",
		"templates",
		"enableHTML",
		"dropRight",
		"buttonWidth",
		"dropRight",
		"dropUp",
		"maxHeight",
		"selectAllNumber",
		"selectAllJustVisible",
		"buttonWidth",
		"enableFullValueFiltering",
		"id",
		"maxHeight"
	];
}
